package com.uh2.fsac.ced.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uh2.fsac.ced.model.Employee;

import java.util.Optional;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    void deleteEmployeeById(Long id);

    Optional<Employee> findEmployeeById(Long id);
}
